package com.hoomi.babylontest;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hoomi.babylontest.mvp.presenter.PostDetailsPresenter;
import com.hoomi.babylontest.mvp.repository.AppRepository;
import com.hoomi.babylontest.mvp.view.PostDetailsView;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsDetailFragment extends Fragment implements PostDetailsView {

    public static final String ARG_POST_ID = "item_id";
    public static final String ARG_POST_USER_ID = "user_id";
    public static final String ARG_POST_TITLE = "title";
    public static final String ARG_POST_BODY = "body";

    @BindView(R.id.postsBodyTextView)
    TextView postBodyTextView;
    @BindView(R.id.userNameTextView)
    TextView postUserNameTextView;
    @BindView(R.id.userAvatarImageView)
    AppCompatImageView postUserAvatarImageView;
    @BindView(R.id.commnetsTextView)
    TextView postCommentTextView;
    CollapsingToolbarLayout appBarLaout;

    //TODO inject the presenter
    private PostDetailsPresenter presenter;

    public PostsDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new PostDetailsPresenter(this, new AppRepository(), getArguments());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.posts_detail, container, false);
        ButterKnife.bind(this, rootView);
        if (getActivity().findViewById(R.id.toolbar_layout) != null) {
            appBarLaout = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
        }
        presenter.loadData();
        return rootView;
    }


    @Override
    public void showUserImage(User user) {
        postUserNameTextView.setText(String.format(Locale.ENGLISH, "Username: %s", user.getUsername()));
        Picasso.with(getContext()).load(user.getAvatarUrl()).into(postUserAvatarImageView);

    }

    @Override
    public void showNumberOfComments(int numberOfComments) {
        postCommentTextView.setText(String.format(Locale.ENGLISH, "Comments: %d", numberOfComments));
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showResult(Post results) {
        postBodyTextView.setText(results.getBody());
        if (appBarLaout != null) {
            appBarLaout.setTitle(results.getTitle());
        }

    }

    @Override
    public void showError(String error) {
        postBodyTextView.setText(error);
        appBarLaout.setTitle("There was an error getting the post details");
    }
}
