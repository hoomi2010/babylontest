package com.hoomi.babylontest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hoomi.babylontest.R;
import com.hoomi.interfaces.models.Post;

import java.util.List;

import butterknife.BindView;

/**
 * Created by hooman on 12/06/16.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private final Context context;
    private List<Post> posts;
    private final ItemClickedListener itemClickedListener;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (itemClickedListener != null) {
                int position = (int) v.getTag(R.id.position);
                itemClickedListener.itemClicked(posts.get(position));
            }

        }
    };

    public PostsAdapter(Context context, List<Post> posts, ItemClickedListener itemClickedListener) {
        this.context = context;
        this.posts = posts;
        this.itemClickedListener = itemClickedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.posts_list_content, parent, false);

        view.setOnClickListener(onClickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.post = posts.get(position);
        holder.titleView.setText(posts.get(position).getTitle());
        holder.itemView.setTag(R.id.position, position);
    }

    @Override
    public int getItemCount() {
        return posts == null ? 0 : posts.size();
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView titleView;
        Post post;

        public ViewHolder(View view) {
            super(view);
            titleView = (TextView) view;
        }


    }

    public interface ItemClickedListener {
        void itemClicked(Post post);
    }
}
