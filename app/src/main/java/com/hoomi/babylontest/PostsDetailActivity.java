package com.hoomi.babylontest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


public class PostsDetailActivity extends AppCompatActivity {

    public static final String ARG_POST_BUNDLE = "args_bundle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        //
        if (savedInstanceState == null) {

            PostsDetailFragment fragment = new PostsDetailFragment();
            fragment.setArguments(getIntent().getBundleExtra(ARG_POST_BUNDLE));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.posts_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, PostsListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
