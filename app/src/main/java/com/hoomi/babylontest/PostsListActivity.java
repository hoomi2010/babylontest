package com.hoomi.babylontest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hoomi.babylontest.adapters.PostsAdapter;
import com.hoomi.babylontest.mvp.presenter.MVPPresenter;
import com.hoomi.babylontest.mvp.presenter.PostsPresenter;
import com.hoomi.babylontest.mvp.repository.AppRepository;
import com.hoomi.babylontest.mvp.view.MVPView;
import com.hoomi.interfaces.models.Post;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsListActivity extends AppCompatActivity implements MVPView<List<Post>>, PostsAdapter.ItemClickedListener {

    private boolean mTwoPane;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.posts_list)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.errorTextView)
    AppCompatTextView errorTextView;

    //TODO Inject this using Dagger 2
    private MVPPresenter postsPresenter;

    private PostsAdapter postsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts_list);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        toolbar.setTitle("List Of Posts");

        mTwoPane = findViewById(R.id.posts_detail_container) != null;

        postsPresenter = new PostsPresenter(this, new AppRepository());
        postsPresenter.loadData();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        errorTextView.setVisibility(View.GONE);

    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showResult(List<Post> results) {
        if (postsAdapter == null) {
            postsAdapter = new PostsAdapter(this, results, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(postsAdapter);
        } else {
            postsAdapter.setPosts(results);
        }
    }

    @Override
    public void showError(String error) {
        errorTextView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }


    @Override
    public void itemClicked(Post post) {

        Bundle arguments = new Bundle();
        arguments.putInt(PostsDetailFragment.ARG_POST_ID, post.getId());
        arguments.putInt(PostsDetailFragment.ARG_POST_USER_ID, post.getId());
        arguments.putString(PostsDetailFragment.ARG_POST_TITLE, post.getTitle());
        arguments.putString(PostsDetailFragment.ARG_POST_BODY, post.getBody());
        if (mTwoPane) {
            PostsDetailFragment fragment = new PostsDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.posts_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, PostsDetailActivity.class);
            intent.putExtra(PostsDetailActivity.ARG_POST_BUNDLE, arguments);

            startActivity(intent);
        }


    }
}
