package com.hoomi.babylontest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.hoomi.babylontest.BuildConfig;

/**
 * Created by hooman on 12/06/16.
 */
//TODO finish off the implementation
public class PostSqliteDatabaseHelper extends SQLiteOpenHelper {

    // TODO Complete the database query string. We need to use foreign keys so that we can easily
    // query comments related to each post, or users related to each post
    private static final String CREATE_QUERY = "CREATE TABLE .....";
    private static final String SQLITE_NAME = "posts.db";

    public PostSqliteDatabaseHelper(Context context) {
        super(context, SQLITE_NAME, null, BuildConfig.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            // TODO most likely we drop the previous tabl;es unless the data structure has not changed
        }
    }
}
