package com.hoomi.babylontest.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
//TODO write the implementation
public class PostDatabaseManager implements DatabaseManager {

    SQLiteDatabase db;

    //TODO use dagger to inject it. Makes it easier to use the application context
    PostSqliteDatabaseHelper postSqliteDatabaseHelper;

    @Override
    public void insertUsers(List<User> userList) {
        //TODO open and close the database
        // Use transaction in case something went wrong while the data modification is being executed

        db.beginTransaction();
        for (User user : userList) {
            ContentValues contentValues = new ContentValues();
            //TODO create the correct contentvalues here
            contentValues.put("name", user.getName());
            db.insert("user", null, contentValues);
            db.yieldIfContendedSafely();
        }
        db.setTransactionSuccessful();
        db.endTransaction();

    }

    //TODO similar to the user above
    @Override
    public void insertComments(List<Comment> commentList) {
        //TODO open and close the database

    }

    //TODO similar to the user above
    @Override
    public void insertPosts(List<Post> postList) {
        //TODO open and close the database
    }

    @Override
    public User getUser(int userId) {
        open();
        Cursor cursor = db.query("user", null, "id=?", new String[]{userId + ""}, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                //TODO extract the user from the cursor and then return it at the end of the function
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        close();
        return null;
    }

    //TODO similar to the user above. The only difference is that this is a list
    @Override
    public List<Comment> getComments(int postId) {
        return null;
    }


    private SQLiteDatabase open() {
        //TODO open the database. Check if there is one already open, use application context
//        db = new PostSqliteDatabaseHelper().getWritableDatabase();
        return db;
    }


    private void close() {
        // TODO check if the database instance is open close it
        // TODO make sure that there is a mechanism which checks to see if the database has been acquired
        // by ither threads so we do not close the database in the middle of their operation
        if (db != null) {
            db.isOpen();
            db = null;
        }

    }
}
