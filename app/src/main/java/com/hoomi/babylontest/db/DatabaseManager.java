package com.hoomi.babylontest.db;

import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
//TODO do the implementation of these interface
public interface DatabaseManager {
    void insertUsers(List<User> userList);

    void insertComments(List<Comment> commentList);

    void insertPosts(List<Post> postList);

    User getUser(int userId);

    List<Comment> getComments(int postId);
}
