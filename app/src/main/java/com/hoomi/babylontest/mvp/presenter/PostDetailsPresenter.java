package com.hoomi.babylontest.mvp.presenter;

import android.os.Bundle;

import com.hoomi.babylontest.model.AppPost;
import com.hoomi.babylontest.mvp.repository.MVPModel;
import com.hoomi.babylontest.mvp.view.PostDetailsView;
import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public class PostDetailsPresenter implements MVPPresenter, UserCallback, CommentFilteredCallback {

    private final Post post;
    private PostDetailsView mvpView;
    private MVPModel repository;

    public PostDetailsPresenter(PostDetailsView mvpView, MVPModel repository, Bundle bundle) {
        this(mvpView, repository, new AppPost(bundle));
    }

    PostDetailsPresenter(PostDetailsView mvpView, MVPModel repository, Post post) {
        this.mvpView = mvpView;
        this.repository = repository;
        this.post = post;
    }

    @Override
    public void loadData() {
        // We already have the post so call it on the view
        mvpView.showResult(post);
        repository.getUser(post.getUserId(), this);
        repository.getComments(post.getId(), this);

    }

    @Override
    public void onUserSuccess(User user) {
        mvpView.showUserImage(user);
    }

    @Override
    public void onUserError(String error) {
        //TODO I have not decided what to do with this yet
    }

    @Override
    public void onCommentsFilteredSuccessfully(List<Comment> commentList) {
        mvpView.showNumberOfComments(commentList.size());
    }

    @Override
    public void onCommentsError(String errorMessage) {
        //TODO I have not decided what to do with this yet

    }
}
