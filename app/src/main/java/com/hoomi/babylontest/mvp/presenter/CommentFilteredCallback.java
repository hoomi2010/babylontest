package com.hoomi.babylontest.mvp.presenter;

import com.hoomi.interfaces.models.Comment;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public interface CommentFilteredCallback {
    void onCommentsFilteredSuccessfully(List<Comment> commentList);

    void onCommentsError(String errorMessage);
}
