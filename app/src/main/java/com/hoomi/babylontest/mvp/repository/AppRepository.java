package com.hoomi.babylontest.mvp.repository;

import android.support.annotation.VisibleForTesting;

import com.hoomi.babylontest.db.DatabaseManager;
import com.hoomi.babylontest.db.PostDatabaseManager;
import com.hoomi.babylontest.mvp.presenter.CommentFilteredCallback;
import com.hoomi.babylontest.mvp.presenter.PostCallback;
import com.hoomi.babylontest.mvp.presenter.UserCallback;
import com.hoomi.interfaces.PostsService;
import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;
import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;
import com.hoomi.posts.RetrofitPostsService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
// TODO this should be a singleton
public class AppRepository implements MVPModel {


    private final PostsService postsService;
    //TODO inject using dagger 2
    private final DatabaseManager databaseManager;

    public AppRepository() {
        this(RetrofitPostsService.getInstance(), new PostDatabaseManager());
    }

    @VisibleForTesting
    AppRepository(PostsService postsService, DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        this.postsService = postsService;
    }

    @Override
    public void getPosts(final PostCallback postCallback) {
        //TODO read the posts from the database if they are already downloaded
        this.postsService.getPosts(new PostsCallback() {
            @Override
            public void onSuccess(List<Post> posts) {
                // TODO As the data is static try to cache them here using the databasemanager
                if (postCallback != null) {
                    postCallback.onSuccess(posts);
                }
            }

            @Override
            public void onFailure(String errorMessage) {
                if (postCallback != null) {
                    postCallback.onError(errorMessage);
                }
            }
        });
    }

    @Override
    public void getUser(final int userId, final UserCallback userCallback) {
        //TODO easily read the data from the databse using the sqlite query langugage. There is no need for filtering in java layer
        this.postsService.getUsers(new UsersCallback() {
            @Override
            public void onSuccess(List<User> users) {
                // TODO As the data is static try to cache them here using the databasemanager
                if (userCallback != null) {
                    User user = getTheCorrectUser(userId, users);
                    if (user != null) {
                        userCallback.onUserSuccess(user);
                    } else {
                        userCallback.onUserError("User could not be found");
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {
                if (userCallback != null) {
                    userCallback.onUserError(errorMessage);
                }
            }
        });
    }

    @Override
    public void getComments(final int postId, final CommentFilteredCallback commentFilteredCallback) {
        //TODO easily read the comments from the database using the sqlite query langugage. There is no need for filtering in java layer
        this.postsService.getComments(new CommentsCallback() {
            @Override
            public void onSuccess(List<Comment> rawComments) {
                // TODO As the data is static try to cache them here
                if (commentFilteredCallback != null) {
                    List<Comment> comments = filterRelatedComments(postId, rawComments);
                    commentFilteredCallback.onCommentsFilteredSuccessfully(filterRelatedComments(postId, comments));
                }
            }

            @Override
            public void onError(String errorMessage) {
                if (commentFilteredCallback != null) {
                    commentFilteredCallback.onCommentsError(errorMessage);
                }
            }
        });
    }


    private User getTheCorrectUser(int userId, List<User> users) {
        if (users != null && users.size() > 0) {
            for (User user : users) {
                if (user.getId() == userId) {
                    return user;
                }
            }
        }
        return null;
    }

    private List<Comment> filterRelatedComments(int commentId, List<Comment> comments) {
        if (comments != null && comments.size() > 0) {
            List<Comment> filteredComments = new ArrayList<>(comments.size());
            for (Comment comment :
                    comments) {
                if (comment.getPostId() == commentId) {
                    filteredComments.add(comment);
                }
            }
            return filteredComments;
        }
        return null;
    }
}
