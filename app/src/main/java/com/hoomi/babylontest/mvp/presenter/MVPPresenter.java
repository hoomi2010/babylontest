package com.hoomi.babylontest.mvp.presenter;

/**
 * Created by hooman on 12/06/16.
 */
public interface MVPPresenter {
    void loadData();
}
