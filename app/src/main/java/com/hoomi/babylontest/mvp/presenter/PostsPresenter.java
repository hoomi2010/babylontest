package com.hoomi.babylontest.mvp.presenter;

import com.hoomi.babylontest.mvp.repository.MVPModel;
import com.hoomi.babylontest.mvp.view.MVPView;
import com.hoomi.interfaces.models.Post;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public class PostsPresenter implements MVPPresenter, PostCallback {

    private final MVPView<List<Post>> mvpView;
    private final MVPModel mvpModel;

    public PostsPresenter(MVPView<List<Post>> mvpView, MVPModel mvpModel) {
        this.mvpView = mvpView;
        this.mvpModel = mvpModel;
    }

    @Override
    public void loadData() {
        mvpView.showProgress();
        mvpModel.getPosts(this);
    }

    @Override
    public void onSuccess(List<Post> posts) {
        mvpView.showResult(posts);
        mvpView.hideProgress();
    }

    @Override
    public void onError(String error) {
        mvpView.showError(error);
        mvpView.hideProgress();
    }
}
