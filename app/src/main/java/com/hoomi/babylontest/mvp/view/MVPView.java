package com.hoomi.babylontest.mvp.view;

/**
 * Created by hooman on 12/06/16.
 */
public interface MVPView<T> {
    void showProgress();

    void hideProgress();

    void showResult(T results);

    //TODO for simplicity I am using a string
    void showError(String error);

}
