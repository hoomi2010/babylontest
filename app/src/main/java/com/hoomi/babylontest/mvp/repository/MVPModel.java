package com.hoomi.babylontest.mvp.repository;

import com.hoomi.babylontest.mvp.presenter.CommentFilteredCallback;
import com.hoomi.babylontest.mvp.presenter.PostCallback;
import com.hoomi.babylontest.mvp.presenter.UserCallback;

/**
 * Created by hooman on 12/06/16.
 */
public interface MVPModel {
    void getPosts(PostCallback postCallback);

    void getUser(int userId, UserCallback userCallback);

    void getComments(int postId, CommentFilteredCallback commentFilteredCallback);
}
