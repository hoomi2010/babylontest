package com.hoomi.babylontest.mvp.presenter;

import com.hoomi.interfaces.models.User;

/**
 * Created by hooman on 12/06/16.
 */
public interface UserCallback {
    void onUserSuccess(User user);

    void onUserError(String error);
}
