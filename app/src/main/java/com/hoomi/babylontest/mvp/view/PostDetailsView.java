package com.hoomi.babylontest.mvp.view;

import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

/**
 * Created by hooman on 12/06/16.
 */
public interface PostDetailsView extends MVPView<Post> {

    void showUserImage(User user);

    void showNumberOfComments(int numberOfComments);
}
