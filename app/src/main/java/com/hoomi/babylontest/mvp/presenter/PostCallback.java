package com.hoomi.babylontest.mvp.presenter;

import com.hoomi.interfaces.models.Post;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public interface PostCallback {
    void onSuccess(List<Post> posts);

    void onError(String error);
}
