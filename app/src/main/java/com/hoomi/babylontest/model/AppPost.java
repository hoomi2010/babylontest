package com.hoomi.babylontest.model;

import android.os.Bundle;

import com.hoomi.babylontest.PostsDetailFragment;
import com.hoomi.interfaces.models.Post;

/**
 * Created by hooman on 12/06/16.
 */
public class AppPost implements Post {

    private int userId;
    private int id;
    private String title;
    private String body;


    public AppPost(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public AppPost(Bundle bundle) {
        this.userId = bundle.getInt(PostsDetailFragment.ARG_POST_USER_ID, -1);
        this.id = bundle.getInt(PostsDetailFragment.ARG_POST_ID, -1);
        this.title = bundle.getString(PostsDetailFragment.ARG_POST_TITLE, "");
        this.body = bundle.getString(PostsDetailFragment.ARG_POST_BODY, "");
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getBody() {
        return body;
    }
}
