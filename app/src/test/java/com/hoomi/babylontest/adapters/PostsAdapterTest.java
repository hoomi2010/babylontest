package com.hoomi.babylontest.adapters;

import android.content.Context;

import com.hoomi.interfaces.models.Post;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by hooman on 12/06/16.
 */
public class PostsAdapterTest {

    @Mock
    private Context mockedContext;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetItemCount_should_return_zero_for_a_null_list() throws Exception {
        PostsAdapter postsAdapter = new PostsAdapter(mockedContext, null, null);

        assertThat(postsAdapter.getItemCount(), is(0));

    }

    @Test
    public void testGetItemCount_should_return_zero_for_an_empty_list() throws Exception {
        PostsAdapter postsAdapter = new PostsAdapter(mockedContext, new ArrayList<Post>(), null);

        assertThat(postsAdapter.getItemCount(), is(0));

    }
}