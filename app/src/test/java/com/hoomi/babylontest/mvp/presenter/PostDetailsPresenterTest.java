package com.hoomi.babylontest.mvp.presenter;

import com.hoomi.babylontest.mvp.repository.MVPModel;
import com.hoomi.babylontest.mvp.view.PostDetailsView;
import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by hooman on 12/06/16.
 */
public class PostDetailsPresenterTest {

    @Mock
    private MVPModel mockedModel;
    @Mock
    private PostDetailsView mockedView;
    @Mock
    private Post mockedPost;

    private PostDetailsPresenter postDetailsPresenter;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        postDetailsPresenter = new PostDetailsPresenter(mockedView, mockedModel, mockedPost);

    }

    @Test
    public void testLoadData_should_call_show_results_directly_on_the_view() throws Exception {
        postDetailsPresenter.loadData();

        verify(mockedView).showResult(mockedPost);
        verify(mockedView, never()).showProgress();
    }

    @Test
    public void testLoadData_should_call_show_call_get_user_on_the_model() throws Exception {
        int TEST_USER_ID = 2;

        when(mockedPost.getUserId()).thenReturn(TEST_USER_ID);

        postDetailsPresenter.loadData();

        verify(mockedModel).getUser(eq(TEST_USER_ID), any(UserCallback.class));
    }

    @Test
    public void testLoadData_should_call_on_user_image_on_the_view() throws Exception {
        int TEST_USER_ID = 2;
        final User expectedUser = mock(User.class);

        when(mockedPost.getUserId()).thenReturn(TEST_USER_ID);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserCallback userCallback = (UserCallback) invocation.getArguments()[1];
                userCallback.onUserSuccess(expectedUser);
                return null;
            }
        }).when(mockedModel).getUser(eq(TEST_USER_ID), any(UserCallback.class));

        postDetailsPresenter.loadData();

        verify(mockedView).showUserImage(expectedUser);
    }

    @Test
    public void testLoadData_should_call_show_call_get_comments_on_the_model() throws Exception {
        int TEST_POST_ID = 2;

        when(mockedPost.getId()).thenReturn(TEST_POST_ID);

        postDetailsPresenter.loadData();

        verify(mockedModel).getComments(eq(TEST_POST_ID), any(CommentFilteredCallback.class));
    }

    @Test
    public void testLoadData_should_call_on_comments_updated_on_the_view() throws Exception {
        int TEST_POST_ID = 2;
        final Comment mockedComment = mock(Comment.class);
        final List<Comment> comments = new ArrayList<>();
        comments.add(mockedComment);
        comments.add(mockedComment);
        comments.add(mockedComment);
        comments.add(mockedComment);

        when(mockedPost.getId()).thenReturn(TEST_POST_ID);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                CommentFilteredCallback commentFilteredCallback = (CommentFilteredCallback) invocation.getArguments()[1];
                commentFilteredCallback.onCommentsFilteredSuccessfully(comments);
                return null;
            }
        }).when(mockedModel).getComments(eq(TEST_POST_ID), any(CommentFilteredCallback.class));

        postDetailsPresenter.loadData();

        verify(mockedView).showNumberOfComments(comments.size());
    }
}