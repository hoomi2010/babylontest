package com.hoomi.babylontest.mvp.presenter;

import com.hoomi.babylontest.mvp.repository.MVPModel;
import com.hoomi.babylontest.mvp.view.MVPView;
import com.hoomi.interfaces.models.Post;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

/**
 * Created by hooman on 12/06/16.
 */
@SuppressWarnings("unchecked")
public class PostsPresenterTest {

    @Mock
    private MVPView mockedView;
    @Mock
    private MVPModel mockedModel;

    private PostsPresenter postsPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        postsPresenter = new PostsPresenter(mockedView, mockedModel);

    }

    @Test
    public void testLoadData_should_call_get_posts_on_the_model() throws Exception {

        postsPresenter.loadData();

        verify(mockedModel).getPosts(any(PostCallback.class));
        verify(mockedView).showProgress();

    }

    @Test
    public void testOnSuccess_should_call_hideProgress_and_show_results_on_view() throws Exception {

        postsPresenter.onSuccess(new ArrayList<Post>());

        verify(mockedView).hideProgress();
        verify(mockedView).showResult(anyList());

    }

    @Test
    public void testOnFailure_should_call_hideProgress_and_show_error_on_view() throws Exception {

        postsPresenter.onError("This is an error");

        verify(mockedView).hideProgress();
        verify(mockedView).showError(anyString());

    }
}