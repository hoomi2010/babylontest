package com.hoomi.babylontest.mvp.repository;

import com.hoomi.babylontest.mvp.presenter.CommentFilteredCallback;
import com.hoomi.babylontest.mvp.presenter.PostCallback;
import com.hoomi.babylontest.mvp.presenter.UserCallback;
import com.hoomi.interfaces.PostsService;
import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;
import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by hooman on 12/06/16.
 */
//TODO Write tests for error cases
@SuppressWarnings("unchecked")
public class AppRepositoryTest {

    @Mock
    private PostsService mockedPostsService;
    @Mock
    private PostCallback mockedPostCallback;
    @Mock
    private UserCallback mockedUserCallback;
    @Mock
    private CommentFilteredCallback mockedCommentFilteredCallback;
    private AppRepository appRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        appRepository = new AppRepository(mockedPostsService);

    }

    @Test
    public void testGetPosts_should_call_posts_service_to_get_the_list_of_posts() throws Exception {
        appRepository.getPosts(null);

        verify(mockedPostsService).getPosts(any(PostsCallback.class));

    }

    @Test
    public void testGetPosts_should_call_the_success_callback_with_data_when_success() throws Exception {
        final List<Post> expectedList = new ArrayList<>();
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                PostsCallback callback = (PostsCallback) invocation.getArguments()[0];
                callback.onSuccess(expectedList);
                return null;
            }
        }).when(mockedPostsService).getPosts(any(PostsCallback.class));

        appRepository.getPosts(mockedPostCallback);

        verify(mockedPostCallback).onSuccess(expectedList);

    }


    @Test
    public void testGetPosts_should_call_the_failure_callback_with_data_when_failure() throws Exception {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                PostsCallback callback = (PostsCallback) invocation.getArguments()[0];
                callback.onFailure("This is a test error message");
                return null;
            }
        }).when(mockedPostsService).getPosts(any(PostsCallback.class));

        appRepository.getPosts(mockedPostCallback);

        verify(mockedPostCallback, never()).onSuccess(anyList());
        verify(mockedPostCallback).onError(anyString());

    }

    @Test
    public void testGetUsers_should_call_the_success_callback_with_data_when_success() throws Exception {
        final List<User> expectedList = new ArrayList<>();
        User mockedUser = mock(User.class);
        User mockedUser2 = mock(User.class);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        int TEST_USER_ID = 2;

        when(mockedUser.getId()).thenReturn(1);
        when(mockedUser2.getId()).thenReturn(TEST_USER_ID);

        expectedList.add(mockedUser);
        expectedList.add(mockedUser2);
        expectedList.add(mockedUser);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UsersCallback callback = (UsersCallback) invocation.getArguments()[0];
                callback.onSuccess(expectedList);
                return null;
            }
        }).when(mockedPostsService).getUsers(any(UsersCallback.class));

        appRepository.getUser(TEST_USER_ID, mockedUserCallback);

        verify(mockedUserCallback).onUserSuccess(userArgumentCaptor.capture());

        assertThat(userArgumentCaptor.getValue().getId(), Is.is(TEST_USER_ID));

    }

    @Test
    public void testGetComments_should_call_the_success_callback_with_data_when_success() throws Exception {
        final List<Comment> expectedList = new ArrayList<>();
        Comment mockedComment = mock(Comment.class);
        Comment mockedComment2 = mock(Comment.class);
        ArgumentCaptor<List<Comment>> commentArgumentCaptor = ArgumentCaptor.forClass(List.class);
        int TEST_POST_ID = 100;

        when(mockedComment.getPostId()).thenReturn(1);
        when(mockedComment2.getPostId()).thenReturn(TEST_POST_ID);

        //TODO replace with for loop
        expectedList.add(mockedComment);
        expectedList.add(mockedComment2);
        expectedList.add(mockedComment);
        expectedList.add(mockedComment);
        expectedList.add(mockedComment);
        expectedList.add(mockedComment);
        expectedList.add(mockedComment2);
        expectedList.add(mockedComment);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                CommentsCallback callback = (CommentsCallback) invocation.getArguments()[0];
                callback.onSuccess(expectedList);
                return null;
            }
        }).when(mockedPostsService).getComments(any(CommentsCallback.class));

        appRepository.getComments(TEST_POST_ID, mockedCommentFilteredCallback);

        verify(mockedCommentFilteredCallback).onCommentsFilteredSuccessfully(commentArgumentCaptor.capture());

        assertThat(commentArgumentCaptor.getValue().size(), Is.is(2));

    }
}