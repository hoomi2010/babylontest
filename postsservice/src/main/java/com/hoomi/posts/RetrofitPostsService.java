package com.hoomi.posts;

import com.hoomi.interfaces.PostsService;
import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;
import com.hoomi.posts.network.RetrofitManager;
import com.hoomi.posts.network.RetrofitManagerImp;

public class RetrofitPostsService implements PostsService {


    private static PostsService singleInstance;
    private RetrofitManager retrofitManager;

    // It is package protected because of testing
    RetrofitPostsService(RetrofitManager retrofitManager) {
        this.retrofitManager = retrofitManager;
    }

    public static PostsService getInstance() {
        if (singleInstance == null) {
            synchronized (RetrofitPostsService.class) {
                if (singleInstance == null) {
                    singleInstance = new RetrofitPostsService(new RetrofitManagerImp());
                }
            }
        }
        return singleInstance;
    }

    @Override
    public void getPosts(PostsCallback postsCallback) {
        retrofitManager.getPosts(postsCallback);
    }

    @Override
    public void getUsers(UsersCallback usersCallback) {
        retrofitManager.getUsers(usersCallback);
    }

    @Override
    public void getComments(CommentsCallback commentsCallback) {
        retrofitManager.getComments(commentsCallback);
    }
}
