package com.hoomi.posts.model;

import java.util.Locale;

/**
 * Created by hooman on 12/06/16.
 */
public class User implements com.hoomi.interfaces.models.User {
    private String email;
    private String username;
    private String name;
    private int id;

    @Override
    public String getAvatarUrl() {
        if (email != null && email.length() > 0) {
            return String.format(Locale.ENGLISH, "https://api.adorable.io/avatars/285/%s.png", email);
        }
        return String.format(Locale.ENGLISH, "https://api.adorable.io/avatars/285/%s.png", "test@gmail.com");
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }
}
