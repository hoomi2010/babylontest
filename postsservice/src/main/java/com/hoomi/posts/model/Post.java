package com.hoomi.posts.model;

/**
 * Created by hooman on 12/06/16.
 */
class Post implements com.hoomi.interfaces.models.Post {

    private int userId;
    private int id;
    private String title;
    private String body;

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getBody() {
        return body;
    }
}
