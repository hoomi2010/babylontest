package com.hoomi.posts.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.hoomi.interfaces.models.Post;

import java.lang.reflect.Type;

/**
 * Created by hooman on 12/06/16.
 */
public class PostDeserializer implements JsonDeserializer {

    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (typeOfT == Post.class) {
            return context.deserialize(json, com.hoomi.posts.model.Post.class);
        } else if (typeOfT == com.hoomi.interfaces.models.User.class) {
            return context.deserialize(json, com.hoomi.posts.model.User.class);
        } else if (typeOfT == com.hoomi.interfaces.models.Comment.class) {
            return context.deserialize(json, com.hoomi.posts.model.Comment.class);
        }
        return context.deserialize(json, typeOfT);
    }
}
