package com.hoomi.posts.model;

/**
 * Created by hooman on 12/06/16.
 */
public class Comment implements com.hoomi.interfaces.models.Comment {
    private int postId;
    private int id;
    private String name;
    private String email;
    private String body;

    @Override
    public int getPostId() {
        return postId;
    }

    @Override
    public int getId() {
        return id;
    }
}
