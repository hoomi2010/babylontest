package com.hoomi.posts.network;

import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;

/**
 * Created by hooman on 12/06/16.
 */
public interface RetrofitManager {
    void getPosts(PostsCallback postsCallback);

    void getUsers(UsersCallback usersCallback);

    void getComments(CommentsCallback commentsCallback);
}
