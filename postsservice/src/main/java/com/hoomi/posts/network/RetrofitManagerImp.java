package com.hoomi.posts.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;
import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;
import com.hoomi.posts.model.PostDeserializer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hooman on 12/06/16.
 */
public class RetrofitManagerImp implements RetrofitManager {

    private RPostsService rPostsService;


    public RetrofitManagerImp() {
        this("http://jsonplaceholder.typicode.com");
    }


    RetrofitManagerImp(String baseUrl) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Post.class, new PostDeserializer())
                .registerTypeAdapter(User.class, new PostDeserializer())
                .registerTypeAdapter(Comment.class, new PostDeserializer())
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl)
                .build();
        rPostsService = retrofit.create(RPostsService.class);
    }

    @Override
    public void getPosts(final PostsCallback postsCallback) {
        rPostsService.getPosts().enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Post> posts = response.body();
                    postsCallback.onSuccess(posts);
                } else {
                    postsCallback.onFailure(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                postsCallback.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void getUsers(final UsersCallback usersCallback) {
        rPostsService.getUsers().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<User> posts = response.body();
                    usersCallback.onSuccess(posts);
                } else {
                    usersCallback.onError(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                usersCallback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void getComments(final CommentsCallback commentsCallback) {
        rPostsService.getComments().enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Comment> posts = response.body();
                    commentsCallback.onSuccess(posts);
                } else {
                    commentsCallback.onError(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                commentsCallback.onError(t.getMessage());
            }
        });
    }
}
