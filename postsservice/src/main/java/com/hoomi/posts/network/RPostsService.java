package com.hoomi.posts.network;

import com.hoomi.interfaces.models.Comment;
import com.hoomi.interfaces.models.Post;
import com.hoomi.interfaces.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by hooman on 12/06/16.
 */
public interface RPostsService {


    @GET("posts")
    Call<List<Post>> getPosts();

    @GET("users")
    Call<List<User>> getUsers();

    @GET("comments")
    Call<List<Comment>> getComments();
}
