package com.hoomi.posts.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hoomi.posts.Constants;
import com.hoomi.posts.DummyClass;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * Created by hooman on 12/06/16.
 */
public class PostDeserializerTest {

    private Gson gson;

    @Before
    public void setUp() throws Exception {
        gson = new GsonBuilder()
                .registerTypeAdapter(com.hoomi.interfaces.models.Post.class, new PostDeserializer())
                .registerTypeAdapter(com.hoomi.interfaces.models.User.class, new PostDeserializer())
                .registerTypeAdapter(com.hoomi.interfaces.models.Comment.class, new PostDeserializer())
                .create();
    }

    @Test
    public void testDeserialize_interface_post_should_return_post_implementation() throws Exception {
        com.hoomi.interfaces.models.Post post = gson.fromJson(Constants.ONE_POST, com.hoomi.interfaces.models.Post.class);

        assertThat(post.getUserId(), Is.is(10));
        assertThat(post.getId(), Is.is(100));
        assertThat(post.getTitle(), Is.is("at nam consequatur ea labore ea harum"));
        assertThat(post.getBody(), Is.is("cupiditate quo est a modi nesciunt soluta\nipsa voluptas error itaque dicta in\nautem qui minus magnam et distinctio eum\naccusamus ratione error aut"));

    }


    @Test
    public void testDeserialize_interface_user_should_return_user_implementation() throws Exception {
        com.hoomi.interfaces.models.User user = gson.fromJson(Constants.ONE_USER, com.hoomi.interfaces.models.User.class);

        assertThat(user.getId(), Is.is(2));
        assertThat(user.getName(), Is.is("Ervin Howell"));
        assertThat(user.getUsername(), Is.is("Antonette"));
        assertThat(user.getAvatarUrl(), Is.is("https://api.adorable.io/avatars/285/Shanna@melissa.tv.png"));

    }

    @Test
    public void testDeserialize_interface_comment_should_return_comment_implementation() throws Exception {
        com.hoomi.interfaces.models.Comment comment = gson.fromJson(Constants.ONE_COMMENT, com.hoomi.interfaces.models.Comment.class);

        assertThat(comment.getId(), Is.is(1));
        assertThat(comment.getPostId(), Is.is(1));
    }

    @Test
    public void testDeserialize_should_not_effect_the_normal_behaviour_for_other_objects() throws Exception {
        DummyClass dummyClass = gson.fromJson("{\"dummyId\": \"100\",\"dummyTitle\":\"Test\"}", DummyClass.class);

        assertThat(dummyClass.getDummyId(), Is.is("100"));
        assertThat(dummyClass.getDummyTitle(), Is.is("Test"));
    }


}