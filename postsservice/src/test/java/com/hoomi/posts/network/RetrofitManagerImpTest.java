package com.hoomi.posts.network;

import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;
import com.hoomi.posts.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by hooman on 12/06/16.
 */
//TODO write the error cases for comments and users
@SuppressWarnings("unchecked")
public class RetrofitManagerImpTest {

    @Mock
    private PostsCallback mockedPostsCallback;
    @Mock
    private UsersCallback mockedUsersCallback;
    @Mock
    private CommentsCallback mockedCommentsCallback;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetPosts_with_mocked_response_should_call_onSuccess_on_posts_callback() throws Exception {
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.enqueue(getMockResponse(Constants.POSTS_REPONSE, true));

        mockWebServer.start();

        HttpUrl httpUrl = mockWebServer.url("posts");
        RetrofitManagerImp retrofitManagerImp = new RetrofitManagerImp(String.format(Locale.ENGLISH, "%s://%s:%d", httpUrl.scheme(), httpUrl.host(), httpUrl.port()));
        retrofitManagerImp.getPosts(mockedPostsCallback);
        Thread.sleep(200);
        verify(mockedPostsCallback).onSuccess(anyList());
        verify(mockedPostsCallback, never()).onFailure(anyString());

        mockWebServer.shutdown();
    }

    @Test
    public void testGetPosts_internal_exception_should_call_failure_with_exception_message() throws Exception {
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.enqueue(getMockResponse("", false));

        mockWebServer.start();

        HttpUrl httpUrl = mockWebServer.url("posts");
        RetrofitManagerImp retrofitManagerImp = new RetrofitManagerImp(String.format(Locale.ENGLISH, "%s://%s:%d", httpUrl.scheme(), httpUrl.host(), httpUrl.port()));
        retrofitManagerImp.getPosts(mockedPostsCallback);
        Thread.sleep(200);
        verify(mockedPostsCallback, never()).onSuccess(anyList());
        verify(mockedPostsCallback).onFailure(anyString());

        mockWebServer.shutdown();
    }

    @Test
    public void testGetUsers_with_mocked_response_should_call_onSuccess_on_users_callback() throws Exception {
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.enqueue(getMockResponse(Constants.POSTS_USER, true));

        mockWebServer.start();

        HttpUrl httpUrl = mockWebServer.url("users");
        RetrofitManagerImp retrofitManagerImp = new RetrofitManagerImp(String.format(Locale.ENGLISH, "%s://%s:%d", httpUrl.scheme(), httpUrl.host(), httpUrl.port()));
        retrofitManagerImp.getUsers(mockedUsersCallback);
        Thread.sleep(200);
        verify(mockedUsersCallback).onSuccess(anyList());
        verify(mockedUsersCallback, never()).onError(anyString());

        mockWebServer.shutdown();
    }


    @Test
    public void testGetComments_with_mocked_response_should_call_onSuccess_on_comments_callback() throws Exception {
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.enqueue(getMockResponse(Constants.POSTS_COMMENTS, true));

        mockWebServer.start();

        HttpUrl httpUrl = mockWebServer.url("comments");
        RetrofitManagerImp retrofitManagerImp = new RetrofitManagerImp(String.format(Locale.ENGLISH, "%s://%s:%d", httpUrl.scheme(), httpUrl.host(), httpUrl.port()));
        retrofitManagerImp.getComments(mockedCommentsCallback);
        Thread.sleep(200);
        verify(mockedCommentsCallback).onSuccess(anyList());
        verify(mockedCommentsCallback, never()).onError(anyString());

        mockWebServer.shutdown();
    }

    private MockResponse getMockResponse(String reponse, boolean httpOk) {
        MockResponse mockResponse = new MockResponse();
        mockResponse.setBody(reponse);
        if (!httpOk) {
            mockResponse.setStatus("404 Not Found");
        }
        return mockResponse;
    }
}