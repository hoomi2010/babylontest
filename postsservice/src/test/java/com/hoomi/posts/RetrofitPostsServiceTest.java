package com.hoomi.posts;

import com.hoomi.interfaces.PostsService;
import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;
import com.hoomi.posts.network.RetrofitManager;

import org.hamcrest.core.IsNull;
import org.hamcrest.core.IsSame;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by hooman on 12/06/16.
 */
public class RetrofitPostsServiceTest {

    @Mock
    private RetrofitManager mockedRetrofitManager;

    private RetrofitPostsService retrofitPostsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        retrofitPostsService = new RetrofitPostsService(mockedRetrofitManager);

    }

    @Test
    public void testGetInstance_should_not_be_null() throws Exception {
        PostsService postsService = RetrofitPostsService.getInstance();

        assertThat(postsService, IsNull.notNullValue());
    }

    @Test
    public void testGetInstance_should_return_a_singleton() throws Exception {
        PostsService firstInstance = RetrofitPostsService.getInstance();
        PostsService secondInstance = RetrofitPostsService.getInstance();

        assertThat(firstInstance, IsSame.sameInstance(secondInstance));

    }


    // This will only work correctly if it run alone. If it is run with other tests the signleton
    // has already been initialised.
    @Test
    public void testGetInstance_should_return_the_same_object_across_different_threads() throws Exception {
        int NUMBER_OF_THREADS = 1000;
        final CountDownLatch countDownLatch = new CountDownLatch(NUMBER_OF_THREADS);
        final PostsService[] postsServicesArray = new PostsService[NUMBER_OF_THREADS];
        final Thread[] threadsArray = new Thread[NUMBER_OF_THREADS];

        // Creating threads to call the method from different threads
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            final int finalI = i;
            threadsArray[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    postsServicesArray[finalI] = RetrofitPostsService.getInstance();
                    countDownLatch.countDown();
                }
            });
        }

        // Start threads here so they start at the same time
        for (Thread thread : threadsArray) {
            thread.start();
        }

        // Wait until all the threads have called the getInstance
        countDownLatch.await(1000, TimeUnit.MILLISECONDS);

        //Check that all the instances of PostsService are the same
        for (PostsService ps :
                postsServicesArray) {
            assertThat(postsServicesArray[0], IsSame.sameInstance(ps));
        }
    }


    @Test
    public void testGetPosts_should_call_get_posts_on_RetrofitManager() throws Exception {

        PostsCallback mockedCallback = mock(PostsCallback.class);

        retrofitPostsService.getPosts(mockedCallback);

        verify(mockedRetrofitManager).getPosts(mockedCallback);

    }

    @Test
    public void testGetUsers_should_call_get_users_on_RetrofitManager() throws Exception {

        UsersCallback mockedCallback = mock(UsersCallback.class);

        retrofitPostsService.getUsers(mockedCallback);

        verify(mockedRetrofitManager).getUsers(mockedCallback);

    }


    @Test
    public void testGetComments_should_call_get_comments_on_RetrofitManager() throws Exception {

        CommentsCallback mockedCallback = mock(CommentsCallback.class);

        retrofitPostsService.getComments(mockedCallback);

        verify(mockedRetrofitManager).getComments(mockedCallback);

    }
}