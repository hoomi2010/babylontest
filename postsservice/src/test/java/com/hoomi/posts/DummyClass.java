package com.hoomi.posts;

/**
 * Created by hooman on 12/06/16.
 */
public class DummyClass {
    private String dummyId;
    private String dummyTitle;

    public String getDummyId() {
        return dummyId;
    }

    public String getDummyTitle() {
        return dummyTitle;
    }
}
