package com.hoomi.interfaces.callback;

import com.hoomi.interfaces.models.Comment;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public interface CommentsCallback {
    void onSuccess(List<Comment> comments);

    void onError(String errorMessage);
}
