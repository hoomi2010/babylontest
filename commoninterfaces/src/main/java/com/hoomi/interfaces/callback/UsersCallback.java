package com.hoomi.interfaces.callback;

import com.hoomi.interfaces.models.User;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public interface UsersCallback {
    void onSuccess(List<User> users);

    void onError(String error);
}
