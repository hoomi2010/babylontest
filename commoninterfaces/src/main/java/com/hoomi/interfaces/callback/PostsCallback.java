package com.hoomi.interfaces.callback;

import com.hoomi.interfaces.models.Post;

import java.util.List;

/**
 * Created by hooman on 12/06/16.
 */
public interface PostsCallback {
    void onSuccess(List<Post> posts);

    //TODO This can be done using an exception. For simplicity I use String
    void onFailure(String errorMessage);
}
