package com.hoomi.interfaces;

import com.hoomi.interfaces.callback.CommentsCallback;
import com.hoomi.interfaces.callback.PostsCallback;
import com.hoomi.interfaces.callback.UsersCallback;

public interface PostsService {
    void getPosts(PostsCallback postsCallback);

    void getUsers(UsersCallback usersCallback);

    void getComments(CommentsCallback commentsCallback);
}
