package com.hoomi.interfaces.models;

/**
 * Created by hooman on 12/06/16.
 */
// TODO for now just use the min required criteria
public interface Comment {

    int getPostId();

    int getId();

}
