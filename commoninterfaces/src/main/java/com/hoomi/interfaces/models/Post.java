package com.hoomi.interfaces.models;

/**
 * Created by hooman on 12/06/16.
 */
public interface Post {

    int getUserId();

    int getId();

    String getTitle();

    String getBody();
}
