package com.hoomi.interfaces.models;

/**
 * Created by hooman on 12/06/16.
 */
public interface User {
    String getAvatarUrl();

    String getUsername();

    String getName();

    int getId();

}
