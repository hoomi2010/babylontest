# Babylon Test App #

It is the solution to the interview question

## # ## Problems ## # ##

1. The database functionality is not fully implemented. I have just left some stubs and comments on how I would be doing it.

2. At the moment the network requests are not cached so for every page there will be at least two network requests

3. I did not get the chance to include dagger 2 in my app but that will be a very nice feature to have.

## Good points ##

1. 90% of the logic is unit tested. Using TDD

2. The app loads the details page with the user avatar generated from the email address 